export interface IProduct{
    id: number
    title: string
    price: number
    description: string
    followers: number
    image: string
    rating:{
        rate: number
        count: number
    }
}

export interface Reasons{
    id: number
    title: string
    description: string
}

export interface CategoryPlatform{
    id: number
    image: string
    description: string
}

export interface OurTeam{
    id: number
    image: string
    name: string
    post: string
}