
import { HomePage } from "./pages/HomePage";
import { ProductPage } from "./pages/ProductPage";
import { Navigation } from "./components/Navigationn";
import { Route, Routes } from "react-router-dom";
import { PlugPage } from "./pages/PlugPage";

function App() {
  return(
    <>
      <Navigation></Navigation>
      <Routes>
        <Route path="/" element = {<HomePage/>}></Route>
        <Route path="/pr" element = {<ProductPage/>}></Route>
        <Route path="/plug" element = {<PlugPage/>}></Route>
      </Routes>
    </>
  )
}

export default App;
