import { Reasons } from "../models";

export const reasons: Reasons[] = [
    {
        "id": 1,
        "title": "Innovation at the Forefront",
        "description": "Engage with cutting-edge technology and creative processes that are shaping the future of entertainment.",
    },
    {
        "id": 2,
        "title": "Creative Collaboration",
        "description": "Be part of a team that values diverse ideas, where every member contributes to the storytelling and design of unique gaming experiences.",
    },
    {
        "id": 3,
        "title": "Collaborative Environment",
        "description": "Game development is inherently collaborative, bringing together diverse teams of artists, designers, programmers, and writers.",
    },
]