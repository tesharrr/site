import { OurTeam } from "../models";

export const team: OurTeam[] = [
    {
        "id": 1,
        "image": "icon/Bill 1.svg",
        "name": "Ryan Mango",
        "post": "Technical Director",
    },
    {
        "id": 2,
        "image": "icon/Beverly 1.svg",
        "name": "Maria Septimus",
        "post": "Art Director",
    },
    {
        "id": 3,
        "image": "icon/Claudia 1.svg",
        "name": "Phillip Korsgaard",
        "post": "Lead QA",
    },
    {
        "id": 4,
        "image": "icon/Avatar 1.svg",
        "name": "Allison Saris",
        "post": "Game Designer",
    },
]