import { CategoryPlatform } from "../models";

export const categoryPlatform: CategoryPlatform[] = [
    {
        "id": 1,
        "description": "Mobile Game Development",
        "image": "icon/mobile.svg",
    },
    {
        "id": 2,
        "description": "PC Game Development",
        "image": "icon/pc.svg",
      },
      {
        "id": 3,
        "description": "PS4 Game Development",
        "image": "icon/ps4.svg",
      },
      {
        "id": 4,
        "description": "AR/VR Solutions",
        "image": "icon/vr.svg",
      },
      {
        "id": 5,
        "description": "AR/ VR design",
        "image": "icon/ar.svg",
      },
      {
        "id": 6,
        "description": "3D Modelings",
        "image": "icon/3dmodeling.svg",
      },
      
]