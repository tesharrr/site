import { Footer } from "../components/footer/footer";
import { InputEmail } from "../components/home_components/input_email";
import { AboutComp } from "../components/product_components/about_comp";
import { ReasonComp } from "../components/product_components/reasons";
import { TeamProj } from "../components/product_components/team";
import { TitleProd } from "../components/product_components/title_product";
import { reasons } from "../data/reason";
import { team } from "../data/team";

export function ProductPage() {
    return(
        <div className="bg-gray-950">
            <TitleProd></TitleProd>
            <div className="container mx-auto flex justify-between max-w-6xl">
                <div className = "text-white text-white font-bold text-xs max-w-6xl sm:text-sm md:text-base lg:text-xl xl:text-2xl ">Why work with us</div>
            </div>
            <div className="container max-w-6xl pt-16 mx-auto flex gap-x-8 items-center justify-center flex-wrap pb-32">
                {reasons.map(reason => <ReasonComp reason = {reason} key={reason.id} /> )}
            </div>
            <AboutComp></AboutComp>
            <div className="container mx-auto flex justify-between max-w-6xl pt-32 text-xs sm:text-sm md:text-base lg:text-xl xl:text-2xl ">
                <div className = "text-white text-white font-bold text-[26px]">Our Team</div>
            </div>
            <div className="container max-w-6xl pt-16 mx-auto flex gap-x-8 items-center justify-center flex-wrap pb-32">
                {team.map(teams => <TeamProj teams = {teams} key={teams.id} /> )}
            </div>
            <InputEmail></InputEmail>
            <div className="h-40"></div>
            <Footer></Footer>
        </div>
    )
}