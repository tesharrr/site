import { Product } from "../components/home_components/Product"
import { Dashboard } from "../components/home_components/Dashboard"
import { products } from "../data/products"
import { buttonAll } from "../components/buttons/button_all"
import { CategoryPlatform } from "../components/home_components/CategoryPlatform"
import { InputEmail } from "../components/home_components/input_email"
import { Footer } from "../components/footer/footer"

export function HomePage() {
  //весь сайт
  return(

    //вызов компонента
    <div className="bg-gray-950 z-negative">
        <div>
            <Dashboard></Dashboard>
        </div>
        <div className="container mx-auto flex justify-between max-w-6xl">
            <div className = "text-white text-white font-bold text-[26px]">Currently Trending Games</div>
            {buttonAll('SEE ALL')}

        </div>
        <div className="container max-w-6xl pt-16 mx-auto flex gap-x-8 items-center justify-center">
            {products.map(product => <Product product = {product} key={product.id} /> )}
        </div>
        <div className="flex justify-center pt-12">
          <p className="text-white text-[35px] w-[600px] text-center pb-8 font-semibold">
            Create memories that will last a lifetime.
          </p>
        </div>
        <div className="flex flex-col items-center">
          <div className="w-[60%] h-[40%]">
            <h1 className="text-white text-[30px] font-semibold">VALORANT</h1>
            <p className="text-white text-[16px] w-[500px] pt-5 pb-12">
              Join millions of players worldwide in VALORANT today. Hone your skills, compete for victory, and make your mark in the annals of first-person shooters.
            </p>
          </div>
          <img className="rounded-md w-[60%] h-[40%]" src="image/valorant2.jpg"/>
        </div>
        <CategoryPlatform></CategoryPlatform>
        <div className="h-40"></div>
        <InputEmail></InputEmail>
        <div className="h-40"></div>
        <Footer></Footer>
    </div>
  )
}