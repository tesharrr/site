
function textFooter(text: string) {
  return(
    <p className="mb-4 text-white">
      <a className="text-white">
        {text}
      </a>
    </p>
  )
}

function iconFooter(img: string) {
  return(
    <img src={img}></img>
  )
}

export function Footer(){
  return (
    <footer className="bg-black text-center text-neutral-600 ">
      <div className="mx-6 py-10 text-center md:text-left">
        <div className="grid-1 grid gap-8 md:grid-cols-2 lg:grid-cols-4">
          {/*Элементы*/}
          <div className="">
          <img src="icon/logo1.png" className="pb-3 "></img>
            <p className="text-white">
            © 2024 Riot Games, Inc. All rights reserved. Riot Games, League of Legends, 
            and any associated logos are trademarks, service marks, and registered trademarks of Riot Games, Inc
            </p>
          </div>
          {/* About us */}
          <div className="pl-10">
            <h6
              className="mb-4 flex justify-center font-semibold uppercase md:justify-start text-white">
              About us
            </h6>
            {textFooter("Zeux")}
            {textFooter("Portfolio")}
            {textFooter("Careers")}
            {textFooter("Contact us")}
          </div>
          {/* Contact us */}
          <div>
            <h6
              className="mb-4 flex justify-center font-semibold uppercase md:justify-start text-white">
              Contact us
            </h6>
            {textFooter("Your feedback is important to us. If you have any questions or suggestions, please contact our support team by email or call us. We are always happy to help!")}
            {textFooter("+ 01 234 567 88")}
          </div>
          <div className="pl-10 flex items-end">
            {iconFooter("icon/Group 5.svg")}
            {iconFooter("icon/Group 9.svg")}
            {iconFooter("icon/Group 10.svg")}
            {iconFooter("icon/Group 11.svg")}
          </div>
        </div>
      </div>

      <div className="containerw-full bg-white h-0.5"></div>

      {/* Копирайт */}
      <div className="bg-black p-6 text-center text-white">
        <span>© 2024 Copyright:&nbsp;</span>
        <a
          className="font-semibold text-white"
        >Lorem All rights Rcerved</a>
      </div>
    </footer>
  )
}