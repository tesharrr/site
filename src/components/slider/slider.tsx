import { Swiper, SwiperSlide } from 'swiper/react';
import { EffectCreative } from 'swiper/modules';
import 'swiper/css';
import 'swiper/css/effect-creative';
import { products } from '../../data/products';


export function Sliderr(){

    const swiperSlides = products.map((product) => (
        <SwiperSlide key={product.id} className=''>
          <img src={`${product.image}`} alt='' />
        </SwiperSlide>
    ));      

    return(
        <Swiper
            className="swiper-container max-w-2xl"
            grabCursor={true}
            effect={'creative'}
            creativeEffect={{
            prev: {
                shadow: true,
                translate: [0, 0, -400],
            },
            next: {
                translate: ['100%', 0, 0],
            },
            }}
            modules={[EffectCreative]}
        >
        {swiperSlides}
      </Swiper>
    )
}