import { OurTeam } from "../../models"

interface TeamProps{
    teams: OurTeam
}

export function TeamProj({teams}: TeamProps){
    return(
        <div className = "flex justify-center items-center rounded-lg" style={{ width: '300px', height: '250px', background: '#161A27'}}>
            <div className="flex flex-col justify-center items-center">
                <img src={teams.image} alt="Team"></img>
                <p className="font-bold text-xl text-white">
                    {teams.name}
                </p>
                <p className="text-white text-base pt-2">
                    {teams.post}
                </p>
            </div>
        </div>
    )
}