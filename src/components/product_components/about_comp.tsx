export function AboutComp(){
    return (
        <div className="grid grid-cols-2 justify-between h-xl" style={{background: '#161A27'}}>
            <div style={{display: 'flex', alignItems: 'flex-end', height: '100%'}}>
                <img src="image/runeterra.jpg" style={{height: '70%'}} className="pl-32"></img>
            </div>
            <div className="flex flex-col items-left w-3/4 pr-10 pt-24">
                <p className="text-white text-xs sm:text-sm md:text-base lg:text-lg xl:text-xl"> 
                    About us
                </p>
                <p className="text-white font-bold text-sm sm:text-base md:text-lg lg:text-2xl xl:text-3xl pt-8"> 
                    Riot Games is renowned for its player-focused games and for pioneering the esports industry.
                </p>
                <p className="text-white text-xs sm:text-sm md:text-base lg:text-lg xl:text-xl pt-8"> 
                    Riot Games is an American video game developer, publisher, and esports tournament organizer known for its flagship title, "League of Legends." Founded in 2006 by Brandon Beck and Marc Merrill, the company has grown to become a significant player in the gaming industry and esports scene.
                </p>
            </div>
        </div>
    )
}