import { buttonOrange } from "../buttons/button_all";
import { Sliderr } from "../slider/slider";

export function TitleProd(){
    return (
        <div className="grid grid-cols-2 bg-gray-950 w-full">
            <div className="flex items-center justify-center h-screen flex-col ">
                <div className="absolute center-0 -pt-2">
                    <div className="flex justify-left pb-5">
                        <p className="text-white text-sm sm:text-base md:text-lg lg:text-xl xl:text-2xl">
                            Home &nbsp;&gt; 
                        </p>
                        <p className="text-orange-500 text-sm sm:text-base md:text-lg lg:text-xl xl:text-2xl">
                            &nbsp; About us
                        </p>
                    </div>
                    <p className="text-white font-bold text-lg max-w-[550px] pb-5 sm:text-xl md:text-2xl lg:text-3xl xl:text-4xl">
                        Embark on a journey of imagination and innovation with GameCrafters.
                    </p>
                    <p className="text-white max-w-[550px] pt-3 pb-10 text-xs sm:text-sm md:text-base lg:text-lg xl:text-xl">
                        GameCrafters Studio pioneers the digital frontier, crafting epic adventures that captivate gamers' hearts and minds. 
                        Our passion for storytelling is matched only by our dedication to creating immersive gameplay experiences.                    
                    </p>
                    {buttonOrange("Get in touch ->")}
                </div>
            </div>
            <div >
                <Sliderr/>
            </div>
        </div>
    )
}