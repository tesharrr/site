import { Reasons } from "../../models";

interface ReasonProps{
    reason: Reasons
}

export function ReasonComp({reason}: ReasonProps){
    return (
        <div className = "flex flex-col items-left mb-7 rounded-lg" style={{ width: '360px', height: '360px', background: '#161A27'}}>
            <div className="p-7">
                <div className = "rounded-md bg-orange-800 w-1/2 pt-1 pb-2">
                    <p className="text-white text-s px-4">
                        {reason.title}
                    </p>
                </div>
                <p className="font-bold text-xl text-white pt-6 pb-2">
                    {reason.title}
                </p>
                <p className="text-white text-base pt-4">
                    {reason.description}
                </p>
            </div>
        </div>
    )
}