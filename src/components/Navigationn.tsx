import { useState } from "react";
import { Link } from "react-router-dom";

export function Navigation(){

    // Состояние для отслеживания активной ссылки
    const [activeLinkId, setActiveLinkId] = useState<string | null>(null);

    // Функция для изменения активной ссылки
    const handleLinkClick = (id: string) => {
        setActiveLinkId(id);
    };

    // Стиль для активной ссылки
    const activeStyle = "border-b border-orange-500 px-3 py-2 border-b-4"

    // Стиль для неактивной ссылки
    const inactiveStyle = "px-3 py-2"

    function Links(link:string, linkNumb: string, page:string){
        return(
            <Link to={link}
                        onClick={() => handleLinkClick(linkNumb)}
                        className={activeLinkId === linkNumb 
                            ? activeStyle 
                            : inactiveStyle
                            }>{page}</Link>
        )
    }
    
    return (
        <nav className="h-[70px] flex px-7 bg-gray-950 items-center text-white">
            <img src='icon/logo1.png' className="h-[55px] flex pr-7"></img>
            <div className="space-x-8">
                {Links('/', 'link1', 'Home')}
                {Links('/pr', 'link2', 'About us')}
                {Links('/plug', 'link3', 'Portfolio')}
                {Links('/plug', 'link4', 'News')}
                <Link to = '/plug' className="rounded-md bg-orange-500 px-3 py-2 text-sm font-medium text-white hover:bg-orange-300 hover:text-white transition duration-500">Contact us</Link>
            </div>
        </nav>
    )
}