export function buttonAll(text: string){
    return(
        <button className="rounded-xl bg-gray-900 text-sm font-medium text-white pt-4 py-4 px-6 hover:bg-gray-700 hover:text-white transition duration-500">
            {text}
        </button>
    )
}

export function buttonOrange(text: string){
    return(
        <button className="rounded-2xl bg-orange-500 text-sm font-medium text-white pt-4 py-4 w-1/2 hover:bg-orange-300 hover:text-white transition duration-500">
            {text}
        </button>
    )
}