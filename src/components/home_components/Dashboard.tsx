import { buttonOrange } from "../buttons/button_all";

export function Dashboard(){
    return (
        <div className="grid grid-cols-2">
            <div className="flex items-center justify-center h-screen flex-col">
                <div className="absolute center-0 p-2">
                    <p className="text-orange-500 font-bold text-[20px]">
                        3D game Dev 
                    </p>
                    <p className="text-white font-bold text-[40px] w-[300px]">
                        Work that we produce for our clients
                    </p>
                    <p className="text-white w-[450px] pt-3">
                        Welcome to DevPlayground, your digital nexus for game creation! Dive into a world where your visions take shape and your games come to life.                 
                    </p>
                    <p className="h-[50px]"></p>
                    {buttonOrange("Get more details")}
                </div> 
            </div>   
            <img src="image/joy_stick 1.svg" className="absolute top-0 right-1" style={{width: '40%'}}></img>
        </div>
    )
}