import { categoryPlatform } from "../../data/category_plat";
import { Card } from "../card";

export function CategoryPlatform(){
    return (
        <>
            <div className="h-40"></div>
            <div className="bg-image w-full bg-cover bg-no-repeat bg-center" style={{ backgroundImage: "url('image/image_category.jpg')" }}>
                <div className="flex flex-col items-center py-40">
                    <h1 className="text-white text-center w-[700px] text-[30px] font-semibold">
                        Embark on your learning journey in game development with us at Riot Games.
                    </h1>
                    <p className="text-white text-center w-[730px] text-[16px] pt-10">
                        Riot Games is not only creating exciting games but also provides unique opportunities for learning and professional growth. With each passing year, more and more talented developers join our team to create history together in the world of the gaming industry.
                    </p>
                    <div className="flex flex-wrap justify-center pt-10">
                        {categoryPlatform.map(card => <Card card = {card} key={card.id} /> )}
                    </div>
                </div>
            </div>
        </>
    )
}