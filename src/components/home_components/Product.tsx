import { useState } from "react"
import { IProduct } from "../../models"

interface ProductProps{
    product: IProduct
}

export function Product({product}: ProductProps){
    const [details, setDetails] = useState(true)

    return (
        <div className = "flex flex-col items-center p-35">
            <button>
                <img src={product.image} className="w-50 rounded-lg" alt = {product.title}></img>
            </button>
            <div className="container mx-auto flex items-center justify-center py-10">
                <img src="icon/Vector.svg"/>
                <p className="font-bold text-white px-4">
                    {product.followers} Followers
                </p>
            </div>
        </div>
    )
}