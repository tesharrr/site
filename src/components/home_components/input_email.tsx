export function InputEmail(){
    return (
        <div className = "flex justify-center">
            <div className = "rounded-lg" style={{ width: '85%', height: '189px', background: '#161A27'}}>
                <div className="flex justify-between items-center px-11">
                    <div className="pt-10">
                        <h2 className="text-[30px] font-semibold text-white">Stay in the loop</h2>
                        <p className="mt-2 text-[17px] w-[480px] text-white">
                            Subscribe to receive the latest news and updates about TDA.
                            We promise not to spam you!
                        </p>
                    </div>
                    <div className="pt-10 flex max-w-md gap-x-4">
                        <label htmlFor="email-address" className="sr-only">
                            Email address
                        </label>
                        <input
                            id="email-address"
                            name="email"
                            type="email"
                            autoComplete="email"
                            required
                            className="min-w-0 rounded-md border-0 bg-white px-4 py-2 text-black ring-1 ring-inset ring-white/10 focus:ring-2 focus:ring-inset focus:ring-indigo-500 sm:text-sm sm:leading-6"
                            placeholder="Enter your email"
                        />
                        <button type="submit" className="flex-none rounded-md bg-orange-500 px-3.5 py-2.5 text-sm font-semibold text-white shadow-sm hover:bg-orange-400 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-500">
                            Subscribe
                        </button>
                    </div>
                </div>
            </div>
        </div>
    )
}