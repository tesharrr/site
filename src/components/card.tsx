import { CategoryPlatform } from "../models"


interface CardProps{
    card: CategoryPlatform
}

export function Card({card}: CardProps){
    return (
        <div className="flex flex-col items-center pt-20" style={{ width: '340px'}}>
            <img src={card.image} className="w-50 rounded-lg"></img>
            <p className="font-bold text-white pt-7 pb-7">
                {card.description}
            </p>
            <button>
                <img src="icon/arrow.svg"/>
            </button>
        </div>
    )
}